# Contact List React Application
This Contact List React application is a dynamic solution developed using React.js, CSS, and Axios. It empowers users to manage and organize their contacts effectively, providing functionalities to fetch, add, update, and delete contacts.

## Key Functionalities:

- **Fetch Contacts**: Retrieve and display users from the API - https://jsonplaceholder.typicode.com/users
- **Add a Contact**: Make a POST call to the above URL and save the contact in React state.
- **Update a Contact**: Make a PUT call to the above URL to update a contact's details.
- **Delete a Contact**: Make a DELETE call to the above URL to remove a contact.

## Technologies Employed:

- **React.js**: Provides the framework for building the web application.
- **CSS**: Facilitates visually pleasing and responsive design.
- **Axios**: Enables HTTP requests for fetching, adding, updating, and deleting contacts.

## Requirements

For development, you will only need Node.js (16+) and a node global package (Npm).

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

  If the installation was successful, you should be able to run the following command.

    $ node --version
    V16.xx.xx

    $ npm --version
    x.xx.xx

## Install Project

    $ git clone https://gitlab.com/bhagwatijoshi325/contact-list-using-react.git
    $ cd contact-list-react
    $ npm install

## Running the project on development

    $ npm start

## Folder Structure
```
[CONTACT-LIST-REACT-APP]
│ .gitignore
│ package-lock.json
│ package.json
│ README.md
│
├───public
│ index.html
│
├───src
│ │ App.js
│ │ index.js
│ │
│ ├───components
│ │ Contact.js
│ │ ContactList.js
│ │ AddContact.js
│ │ UpdateContact.js
│ │
│ ├───services
│ │ api.js
│ │
│ └───styles
│ App.css
│ Contact.css
│
└───tests
App.test.js
Contact.test.js
```

