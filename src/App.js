// Importing necessary libraries and components
import React, { useEffect, useState } from "react";
import { ToastContainer } from "react-toastify";
import { Route, Routes } from 'react-router-dom';

import Navbar from "./components/Navbar";
import Home from "./components/Home";
import AddContact from "./components/AddContact";
import EditContact from "./components/EditContact";
import { useDispatch } from "react-redux";

const App = () => {
    // Using Redux dispatch to send actions
    const dispatch = useDispatch();

    // Using useEffect to fetch contacts from the API when the component mounts
    useEffect(() => {
        const data = []; // Array to store fetched contacts
        const promise = async () => {
            await fetch('https://jsonplaceholder.typicode.com/users/')
                .then((response) => response.json())
                .then((json) => {
                    // Mapping through the JSON response to extract contact details
                    json.map((contact) => {
                        data.push({
                            id: contact.id,
                            name: contact.name,
                            number: contact.phone,
                            email: contact.email
                        });
                    })
                });
            // Dispatching the fetched contacts to the Redux store
            dispatch({ type: 'FETCH_CONTACTS', payload: data });
        };
        promise(); // Executing the async function to fetch contacts
    }, []); // Empty dependency array ensures this runs only once on mount

    return (
        <div className="App">
            <ToastContainer /> {/* Container for displaying toast notifications */}
            <Navbar /> {/* Navbar component */}
            <Routes>
                {/* Defining routes for the application */}
                <Route exact path="/" element={<Home />}></Route>
                <Route path="/add" element={<AddContact />}></Route>
                <Route path="/edit/:id" element={<EditContact />}></Route>
            </Routes>
        </div>
    );
}

// Exporting the App component
export default App;
