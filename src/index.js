// Importing necessary libraries and components
import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';

// Importing Bootstrap and Toastify CSS for styling
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

// Importing React Router for handling routing
import { BrowserRouter as Router } from 'react-router-dom';

// Importing Redux functionalities for state management
import { createStore } from 'redux';
import contactReducer from './redux/reducers/contactReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';

// Creating a Redux store using the contact reducer and enabling Redux DevTools
const store = createStore(contactReducer, composeWithDevTools());

// Selecting the root element where the React app will be mounted
const root = createRoot(document.querySelector('#root'));

// Rendering the App component inside the Redux Provider and React Router
root.render(
    <Provider store={store}> {/* Providing the Redux store to the entire app */}
        <Router> {/* Enabling routing within the app */}
            <App /> {/* Main App component */}
        </Router>
    </Provider>
);
